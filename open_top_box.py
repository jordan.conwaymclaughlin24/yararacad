import yararacad as yc

def otb(x: yc.length=3, y: yc.length=3, z: yc.length=3, thickness: yc.length=1) -> yc.Exportable:
	return yc.cuboid(x,y,z).faces("+Z").shell(thickness, kind='intersection')
