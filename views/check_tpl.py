#!/usr/bin/env python

from collections.abc import Mapping, Iterable
from os import popen as cmd
import sys
import bottle

def empty_value_dict(*keys):
	return {k:k for k in keys}

def get_possible_keywords(filename):
	for line in cmd("grep '[A-z][A-z0-9]*' -Po  %s | sort | uniq" % filename):
		yield line.strip()


if __name__ == "__main__":
	for arg in sys.argv[1:]:
		arg = arg.strip()
		print( bottle.template( arg, **empty_value_dict( *get_possible_keywords(arg))))
