import yararacad
from yararatool import get_parameters, get_model_output, Parameter, parameterise
import bottle
import os

import mug as open_top_box
name, o = get_model_output(open_top_box)

parameterised_model = None


def html_type(type):
	try:
		return{str : 'text', int : 'number'}[type]
	except KeyError:
		return type

def update_values(*params, **kwargs):
	for p in params:
		if p.name in kwargs:
			p.set(kwargs[p.name])
		yield p

def render_model(**kwargs):
	global parameterised_model
	parameterised_model = parameterise(o, *update_values( *get_parameters(o), **kwargs))
	return parameterised_model

def d(**kwargs):
	return kwargs

@bottle.route('/output/<filename>')
def output(filename):
	model = parameterised_model or render_model()
	try:
		yararacad.export(parameterised_model, 'output/%s' % filename)
	except FileNotFoundError:
		os.mkdir("output")
		return output(filename)

	return bottle.static_file(filename, root="output", download=filename)

@bottle.route('/static/<filename>')
def static(filename):
	return bottle.static_file(filename, root="static")

@bottle.view('join')
def join(*args):
	return d(base=args)

@bottle.view('viewport')
def viewport(filename='output/mug.gltf'):
	return d(src=filename) 

@bottle.view('links')
def links(*args):
	return d(base=args)

@bottle.view('form_control')
def form_control(p: Parameter):
	return d(name=p.name, type=html_type(p.type), default=p.value)

@bottle.view('control_panel')
def control_panel(name=None, *params: Parameter):
	return d(
			name=name,
			form_base=join(*[form_control(p) for p in params]),
			links=['output/%s.%s' % (name, ext) for ext in ['stl', 'dxf', 'amf']]
		)

@bottle.route('/')
@bottle.view('base')
def root():
	return d(base=join(
		control_panel(name, *get_parameters(o)),
		viewport()
	))

@bottle.route('/', method='POST')
def post_submit():
	render_model(**bottle.request.forms)
	print(parameterised_model)
	return root()

bottle.run(host='localhost', port=8080)
