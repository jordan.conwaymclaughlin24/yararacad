import yararacad as yc

def max_fillet(*widths):
    return min(*widths) * 0.496

def mug(radius: yc.length=50,
	 	height: yc.length=100,
	 	thickness: yc.length=5,
	 	handle_inner_height: yc.length=70,
	 	handle_inner_width: yc.length=35,
	 	handle_width: yc.length=15,
	 	handle_offset: yc.unit_interval=0.5,
	 	handle_thickness: yc.length=10
) -> yc.Exportable:
	#rules
	# thickess < radius
	# thickness < height
	# handle_inner_height < height - 2*handle_thicknes (assuming offset == 0)
	# handle_thickness < handle_height/2
	# handle_thickness < handle_width/2

	body = yc.cylinder(height, radius)
	ht = handle_thickness
	x_radius = handle_inner_height / 2
	y_radius = handle_inner_width - (handle_inner_width * handle_offset / 2 )
	offset = handle_offset * y_radius

	handle_minor_radius = handle_inner_width - offset
	handle = yc.cq.Workplane("ZY")\
	    .center(0, radius + offset)\
	    .ellipse(x_radius + ht, y_radius + ht)\
	    .extrude(handle_width)\
	    .translate((handle_width/2,0,0))\
	    .ellipse(x_radius, y_radius)\
	    .cutThruAll()

	fillet_radius = max_fillet(
	    thickness,
	    handle_width,
	    handle_thickness,
	    (height - handle_inner_height - 2*ht) / 2
	)

	result = body.union(handle)\
	    .faces(">Z")\
	    .hole((radius-thickness)*2,height-thickness)\
	    .fillet(fillet_radius)

	return result
