{ lib
, buildPythonPackage
, fetchPypi
, sphinx
, nose
}:

buildPythonPackage rec {
  pname = "stl";
  version = "0.0.3";

  src = fetchPypi {
    inherit pname version;
    sha256 = "03a6b58ad52ecf16823b1cf1acf6ef382aa5c0fc1152379f464e284a4f81304b";
  };

  buildInputs = [sphinx nose];

  doCheck = false;

  meta = with lib; {
    description = "Read and write STL 3D geometry files in both the ASCII and the binary flavor";
    homepage = "https://github.com/apparentlymart/python-stl";
    license = licenses.mit;
  };
}
